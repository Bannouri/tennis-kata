<h1 align="center">
🌐 Tennis Kata
</h1>

# Introduction
This is my personal work for the Tennis Kata from codingdojo
[link] : https://codingdojo.org/kata/Tennis/
## Prerequisites
- [Node](https://nodejs.org/en/download/) ^10.0.0
- [npm](https://nodejs.org/en/download/package-manager/)

- $ cd folder   // go to project's folder
- $ npm i       // npm install packages
- $ npm test // run tests with Jest testing framework

